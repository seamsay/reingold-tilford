# Reingold-Tilford

A Rust library for laying out aesthetically pleasing trees (the data structure, not the plant).

Here is an example of the layout this algorithm produces, rendered onto a terminal grid:

```
                                                                    0
                                                                    |
                +---------------------+-----------------------------------+-----------------+---------------------------+
                |                     |                                   |                 |                           |
                1                     2                                   3                 4                           5
                |                                                         |                                             |
            +-------+                             +-----------------+-----------------------------+                     |
            |       |                             |                 |                             |                     |
            6       7                             8                 9                             10                    11
                    |                             |                 |                             |                     |
                    |                             |       +-------------------+                   |                     |
                    |                             |       |                   |                   |                     |
                    12                            13      14                  15                  16                    17
                    |                                     |                   |                   |                     |
    +-------+-------+-------+-------+                     |       +-------+-------+-------+       |                     |
    |       |       |       |       |                     |       |       |       |       |       |                     |
    18      19      20      21      22                    23      24      25      26      27      28                    29
                                                                                  |                                     |
                                                                              +-------+                                 |
                                                                              |       |                                 |
                                                                              30      31                                32
                                                                                      |                                 |
                                                                              +-------+-------+                 +-------+-------+
                                                                              |       |       |                 |       |       |
                                                                              33      34      35                36      37      38
```

## Issues and Feature Requests

Feel free to open issues on the [gitlab repo](https://gitlab.com/seamsay/reingold-tilford/) for any
bugs you find or features you want. I will try to get bugs fixed relatively quickly (emphasis on
_try_), but I can't guarantee that I'll ever work on feature requests (that shouldn't stop you from
opening them though).

### Pull Requests

If you want to do the work for me then that's great, just make sure to use `rustfmt` and follow
[conventional commit guidelines](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) as closely
as possible.

## Examples

Please see the [API documentation](https://docs.rs/reingold-tilford/) for basic examples, or the
[examples folder](https://gitlab.com/seamsay/reingold-tilford/tree/master/examples) for more
in-depth examples.

## References

Most of the algorithm was shamelessly stolen from [this blog
post](https://rachel53461.wordpress.com/2014/04/20/algorithm-for-drawing-trees/) and the
accompanying code by Rachel Lim.

[This other blog post](https://llimllib.github.io/pymag-trees/) by Bill Mill is also worth a read.
