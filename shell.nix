{ pkgs ? import <nixos> {} }:
let
  C_INCLUDE_PATH = with pkgs; stdenv.lib.makeSearchPathOutput "dev" "include" [
    ncurses
  ];
  LIBRARY_PATH = with pkgs; stdenv.lib.makeLibraryPath [
    ncurses
  ];
in
pkgs.mkShell {
  buildInputs = with pkgs; [ rustup ];
  inherit C_INCLUDE_PATH LIBRARY_PATH;
}
