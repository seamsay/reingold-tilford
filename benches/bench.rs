#![feature(test)]

extern crate petgraph;
extern crate reingold_tilford;
extern crate test;

use petgraph::graph;
use test::Bencher;

const DIRS: &'static str = include_str!("../assets/open-source-directory-structure.txt");

struct Graph<'n>(graph::Graph<&'n str, ()>);

impl<'n> reingold_tilford::NodeInfo<graph::NodeIndex> for Graph<'n> {
	type Key = graph::NodeIndex;

	fn key(&self, node: graph::NodeIndex) -> Self::Key {
		node
	}

	fn children(&self, node: graph::NodeIndex) -> reingold_tilford::SmallVec<graph::NodeIndex> {
		self.0.neighbors(node).collect()
	}
}

fn graph_tree<'n>(
	max_total_nodes: usize,
	max_nodes_per_child: usize,
) -> (Graph<'n>, graph::NodeIndex) {
	fn child(graph: &Graph, node: graph::NodeIndex, name: &str) -> Option<graph::NodeIndex> {
		for neighbor in graph.0.neighbors(node) {
			if graph.0.node_weight(neighbor) == Some(&name) {
				return Some(neighbor);
			}
		}

		None
	}

	let mut graph = Graph(graph::Graph::new());
	let root = graph.0.add_node(".");

	let mut total_nodes = 1;
	'total: for line in DIRS.lines() {
		let mut path = line.split('/');
		// Ignore current directory.
		assert_eq!(Some("."), path.next());

		let mut node = root;
		'per_child: for dir in path {
			if let Some(child) = child(&graph, node, dir) {
				node = child;
			} else {
				if total_nodes >= max_total_nodes {
					break 'total;
				}

				if graph.0.neighbors(node).count() >= max_nodes_per_child {
					break 'per_child;
				}

				let child = graph.0.add_node(dir);
				graph.0.add_edge(node, child, ());

				total_nodes += 1;

				node = child;
			}
		}
	}

	(graph, root)
}

// TODO: Macro to generate these.

#[bench]
fn petgraph_10_nodes_up_to_3_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(10, 3);
	assert_eq!(tree.0.node_count(), 10);

	b.iter(|| reingold_tilford::layout(&tree, root));
}

#[bench]
fn petgraph_10_nodes_up_to_inf_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(10, std::usize::MAX);
	assert_eq!(tree.0.node_count(), 10);

	b.iter(|| reingold_tilford::layout(&tree, root));
}

#[bench]
fn petgraph_100_nodes_up_to_10_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(100, 10);
	assert_eq!(tree.0.node_count(), 100);

	b.iter(|| reingold_tilford::layout(&tree, root));
}

#[bench]
fn petgraph_100_nodes_up_to_inf_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(100, std::usize::MAX);
	assert_eq!(tree.0.node_count(), 100);

	b.iter(|| reingold_tilford::layout(&tree, root));
}

#[bench]
fn petgraph_1000_nodes_up_to_33_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(1000, 33);
	assert_eq!(tree.0.node_count(), 1000);

	b.iter(|| reingold_tilford::layout(&tree, root));
}

#[bench]
fn petgraph_1000_nodes_up_to_100_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(1000, 100);
	assert_eq!(tree.0.node_count(), 1000);

	b.iter(|| reingold_tilford::layout(&tree, root));
}

#[bench]
fn petgraph_1000_nodes_up_to_inf_children(b: &mut Bencher) {
	let (tree, root) = graph_tree(1000, std::usize::MAX);
	assert_eq!(tree.0.node_count(), 1000);

	b.iter(|| reingold_tilford::layout(&tree, root));
}
